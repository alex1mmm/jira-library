package ut.ru.matveev.alexey.tutorial.library;

import org.junit.Test;
import ru.matveev.alexey.tutorial.library.api.MyPluginComponent;
import ru.matveev.alexey.tutorial.library.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}