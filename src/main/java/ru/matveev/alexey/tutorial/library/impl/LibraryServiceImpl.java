package ru.matveev.alexey.tutorial.library.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import ru.matveev.alexey.tutorial.library.api.LibraryService;

import javax.inject.Named;

@ExportAsService({LibraryService.class})
@Named
public class LibraryServiceImpl implements LibraryService {
    public String getLibraryMessage() {
        return "jira-library test message";
    }
}
