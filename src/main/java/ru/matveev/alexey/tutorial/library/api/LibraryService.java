package ru.matveev.alexey.tutorial.library.api;

public interface LibraryService {
    String getLibraryMessage();
}
