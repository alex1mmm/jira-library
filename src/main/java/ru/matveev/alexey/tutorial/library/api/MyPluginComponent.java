package ru.matveev.alexey.tutorial.library.api;

public interface MyPluginComponent
{
    String getName();
}